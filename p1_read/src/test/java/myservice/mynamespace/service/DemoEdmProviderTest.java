package myservice.mynamespace.service;

import org.apache.olingo.commons.api.edm.FullQualifiedName;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class DemoEdmProviderTest {

    @Test
    @DisplayName("GIVEN DemoEdmProvider WHEN access NAMESPACE THEN actual is expected.")
    void givenDemoEdmProvider_whenAccessNameSpace_thenActualIsExpected() {
        String actual = DemoEdmProvider.NAMESPACE;
        String expected = "OData.Demo";
        assertThat(actual, is(expected));
    }

    @Test
    @DisplayName("GIVEN DemoEdmProvider WHEN access CONTAINER_NAME THEN actual is expected.")
    void givenDemoEdmProvider_whenAccessContainer_Name_thenActualIsExpected() {
        String actual = DemoEdmProvider.CONTAINER_NAME;
        String expected = "Container";
        assertThat(actual, is(expected));
    }

    @Test
    @DisplayName("GIVEN DemoEdmProvider WHEN access CONTAINER FQN THEN actual is expected.")
    void givenDemoEdmProvider_whenAccessContainerFQN_thenActualIsExpected() {
        FullQualifiedName actual = DemoEdmProvider.CONTAINER;
        FullQualifiedName expected = new FullQualifiedName(
                DemoEdmProvider.NAMESPACE,
                DemoEdmProvider.CONTAINER_NAME);
        assertThat(actual, is(expected));
    }

}